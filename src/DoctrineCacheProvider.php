<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache\DoctrineAdapter;


use Doctrine\Common\Cache\CacheProvider;
use Eventize\Cache\DataAdapterInterface;
use Eventize\Cache\CacheProviderInterface;
use Eventize\Cache\DataAdapterTagged;
use Eventize\Cache\Exception\CacheItemPoolExistItemException;
use Psr\Cache\CacheItemInterface;

class DoctrineCacheProvider implements CacheProviderInterface
{
    /**
     * @var CacheProvider
     */
    private $cacheProvider;

    /**
     * @var string
     */
    private $itemClassName;

    /**
     * @var CacheItemInterface[]
     */
    private $deferredItems = [];

    /**
     * DoctrineCacheProvider constructor.
     * @param CacheProvider $cacheProvider
     * @param string $itemClassName
     * @param string $domain
     */
    public function __construct(
        CacheProvider $cacheProvider,
        $itemClassName = DataAdapterTagged::class,
        $domain = null
    )
    {
        $this->cacheProvider = $cacheProvider;
        $this->cacheProvider->setNamespace($domain);

        if (!is_subclass_of($itemClassName, DataAdapterInterface::class)) {
            throw new \InvalidArgumentException('Incorrect cache item class name');
        }

        $this->itemClassName = $itemClassName;
    }

    public function getDomain()
    {
        return $this->cacheProvider->getNamespace();
    }

    /**
     * @inheritdoc
     */
    public function createItem($key) : DataAdapterInterface
    {
        return new $this->itemClassName($key, $this->getDomain());
    }

    /**
     * Create DataAdapterInterface collection for $keys
     *
     * @param array $keys
     * @return DataAdapterInterface[]
     */
    public function createItems($keys) : array
    {
        $collection = [];
        foreach ($keys as $key) {
            $collection[$key] = $this->createItem($key);
        }

        return $collection;
    }

    /**
     * @inheritdoc
     */
    public function getItem($key) : DataAdapterInterface
    {
        if ($value = $this->cacheProvider->fetch($key)) {
            return new $this->itemClassName($key, $this->cacheProvider->getNamespace(), true, $value);
        } else {
            return $this->createItem($key);
        }
    }

    /**
     * @inheritdoc
     */
    public function getItems(array $keys = []) : array
    {
        $found = $this->cacheProvider->fetchMultiple($keys);

        $collection = [];
        foreach ($keys as  $key) {
            $collection[$key] = isset($found[$key])
                ? new $this->itemClassName($key, $this->cacheProvider->getNamespace(), true, $found[$key])
                : $this->createItem($key);
        }

        return $collection;
    }

    /**
     * @inheritdoc
     */
    public function hasItem($key) : bool
    {
        return $this->cacheProvider->contains($key);
    }

    /**
     * @inheritdoc
     */
    public function clear() : bool
    {
        return $this->cacheProvider->flushAll();
    }

    /**
     * @inheritdoc
     */
    public function deleteItem($key) : bool
    {
        return $this->cacheProvider->delete($key);
    }

    /**
     * @inheritdoc
     */
    public function deleteItems(array $keys) : bool
    {
        $success = true;
        foreach ($keys as $key) {
            $success = $this->cacheProvider->delete($key) && $success;
        }

        return $success;
    }

    /**
     * @inheritdoc
     */
    public function save(CacheItemInterface $item) : bool
    {
        return $item instanceof DataAdapterInterface
            ? $this->cacheProvider->save($item->getKey(), json_encode($item), $item->getTTL())
            : $this->cacheProvider->save($item->getKey(), json_encode($item->get()));
    }

    /**
     * @inheritdoc
     */
    public function saveDeferred(CacheItemInterface $item)
    {
        if (isset($this->deferredItems[$item->getKey()])) {
            throw new CacheItemPoolExistItemException();
        }

        $this->deferredItems[$item->getKey()] = $item;

        return true;
    }

    /**
     * @inheritdoc
     */
    public function commit() : bool
    {
        $success = true;
        if ($this->deferredItems) {
            $ttlMap = [];
            foreach ($this->deferredItems as $item) {
                if ($item instanceof DataAdapterInterface) {
                    $ttlMap[(int)$item->getTTL()][$item->getKey()] = json_encode($item);
                } else {
                    $ttlMap[0][$item->getKey()] = json_encode($item);
                }
            }

            foreach ($ttlMap as $ttl => $keyAndValues) {
                $success = $this->cacheProvider->saveMultiple($keyAndValues, $ttl) && $success;
            }

            if ($success) {
                $this->deferredItems = [];
            }
        }

        return $success;
    }
}