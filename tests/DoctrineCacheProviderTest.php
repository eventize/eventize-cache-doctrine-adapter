<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Doctrine\Common\Cache\ArrayCache;
use Eventize\Cache\CacheTagManager;
use Eventize\Cache\DataAdapterInterface;
use Eventize\Cache\DataAdapterTagged;
use Eventize\Cache\DataAdapterTaggedInterface;
use Eventize\Cache\DoctrineAdapter\DoctrineCacheProvider;

class DoctrineCacheProviderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Doctrine\Common\Cache\CacheProvider
     */
    private $arrayCacheProvider;

    /**
     * @var DoctrineCacheProvider
     */
    private $cacheProvider;

    /**
     * @var CacheTagManager
     */
    private $cacheTagManager;

    /**
     * Use ArrayCache for tests
     */
    protected function setUp()
    {
        $this->arrayCacheProvider = new ArrayCache();
        $this->cacheProvider = new DoctrineCacheProvider($this->arrayCacheProvider, DataAdapterTagged::class, 'TestCase');
        $this->cacheTagManager = new CacheTagManager($this->cacheProvider, $this->cacheProvider);
    }

    /**
     * Clear cached data
     */
    protected function tearDown()
    {
        $this->arrayCacheProvider = null;
        $this->cacheProvider = null;
        $this->cacheTagManager = null;
    }

    /**
     * Simple create item
     */
    public function testCreateItem()
    {
        $cacheItem = $this->cacheTagManager->createItem('test-key');
        $this->assertInstanceOf(DataAdapterInterface::class, $cacheItem);
    }

    /**
     * Single item workflow
     */
    public function testSingleItem()
    {
        // Miss + namespace version miss
        $cacheItem = $this->cacheTagManager->getItem('test-item');
        $this->assertInstanceOf(DataAdapterInterface::class, $cacheItem);
        $this->assertFalse($cacheItem->isHit());

        $cacheItem->set('test string');
        $this->assertTrue($this->cacheTagManager->save($cacheItem));

        // Hit
        $secondCacheItem = $this->cacheTagManager->getItem('test-item');
        $this->assertTrue($secondCacheItem->isHit());

        $this->assertEquals('test string', $secondCacheItem->get());
        $this->assertEquals('TestCase', $secondCacheItem->getDomain());

        $cacheItem->set('new data');
        $this->assertEquals('test string', $secondCacheItem->get());
        unset($secondCacheItem, $cacheItem);

        // Miss
        $this->cacheTagManager->deleteItem('test-item');
        $cacheItem = $this->cacheTagManager->getItem('test-item');
        $this->assertFalse($cacheItem->isHit());

        $stats = $this->arrayCacheProvider->getStats();
        $this->assertEquals(1, $stats['hits']);
        $this->assertEquals(3, $stats['misses']);
    }

    /**
     * Test single item with deferred save
     */
    public function testDeferredSingle()
    {
        $this->cacheTagManager->saveDeferred($this->cacheTagManager->createItem('test-item-1'));
        $cacheItem = $this->cacheTagManager->getItem('test-item-1');
        $this->assertFalse($cacheItem->isHit());
        unset($cacheItem);

        $this->assertTrue($this->cacheTagManager->commit());
        $cacheItem = $this->cacheTagManager->getItem('test-item-1');
        $this->assertTrue($cacheItem->isHit());
        unset($cacheItem);
    }

    /**
     * Multiply items workflow
     */
    public function testMultiplyItems()
    {
        // 3 miss + namespace version miss
        /** @var DataAdapterTaggedInterface[] $cacheItemCollection */
        $cacheItemCollection = $this->cacheTagManager->getItems(['test-item-1', 'test-item-2', 'test-item-3']);
        $this->assertFalse($cacheItemCollection['test-item-1']->isHit());
        $this->assertFalse($cacheItemCollection['test-item-2']->isHit());
        $this->assertFalse($cacheItemCollection['test-item-3']->isHit());

        $this->cacheTagManager->save($cacheItemCollection['test-item-1']->set('test-data-1'));
        $this->cacheTagManager->save($cacheItemCollection['test-item-2']->set('test-data-2'));

        // 1 miss + 2 hit
        /** @var DataAdapterTaggedInterface[] $secondCacheItemCollection */
        $secondCacheItemCollection = $this->cacheTagManager->getItems(['test-item-1', 'test-item-2', 'test-item-3']);
        $this->assertTrue($secondCacheItemCollection['test-item-1']->isHit());
        $this->assertTrue($secondCacheItemCollection['test-item-2']->isHit());
        $this->assertFalse($secondCacheItemCollection['test-item-3']->isHit());

        $this->assertEquals('test-data-1', $secondCacheItemCollection['test-item-1']->get());
        $this->assertEquals('test-data-2', $secondCacheItemCollection['test-item-2']->get());
        unset($secondCacheItemCollection);

        $cacheItemCollection['test-item-3']->set('test-data-3');
        $this->cacheTagManager->save($cacheItemCollection['test-item-3']);
        $this->cacheTagManager->deleteItems(['test-item-1', 'test-item-2']);

        // 2 miss + 1 hit
        /** @var DataAdapterTaggedInterface[] $secondCacheItemCollection */
        $secondCacheItemCollection = $this->cacheTagManager->getItems(['test-item-1', 'test-item-2', 'test-item-3']);
        $this->assertFalse($secondCacheItemCollection['test-item-1']->isHit());
        $this->assertFalse($secondCacheItemCollection['test-item-2']->isHit());
        $this->assertTrue($secondCacheItemCollection['test-item-3']->isHit());

        $stats = $this->arrayCacheProvider->getStats();
        $this->assertEquals(3, $stats['hits']);
        $this->assertEquals(7, $stats['misses']);
    }

    /**
     * Test workflow for multiply items and use deferred save
     */
    public function testDeferredMultiply()
    {
        $this->cacheTagManager->saveDeferred($this->cacheTagManager->createItem('test-item-2'));
        $this->cacheTagManager->saveDeferred($this->cacheTagManager->createItem('test-item-3'));
        /** @var DataAdapterTaggedInterface[] $cacheItemCollection */
        $cacheItemCollection = $this->cacheTagManager->getItems(['test-item-2', 'test-item-3']);
        $this->assertFalse($cacheItemCollection['test-item-2']->isHit());
        $this->assertFalse($cacheItemCollection['test-item-3']->isHit());
        unset($cacheItemCollection);

        $this->assertTrue($this->cacheTagManager->commit());
        /** @var DataAdapterTaggedInterface[] $cacheItemCollection */
        $cacheItemCollection = $this->cacheTagManager->getItems(['test-item-2', 'test-item-3']);
        $this->assertTrue($cacheItemCollection['test-item-2']->isHit());
        $this->assertTrue($cacheItemCollection['test-item-3']->isHit());
        unset($cacheItemCollection);
    }

    /**
     * Test tagged items
     */
    public function testTags()
    {
        // 2 miss tags + namespace version miss
        $this->cacheTagManager->save(
            $this->cacheTagManager->createItem('test-item-1')
                ->set('test-data-1')->setTags(['test-tag-1', 'test-tag-2'])
        );

        // 1 hit item + 2 hit tags
        $cacheItem = $this->cacheTagManager->getItem('test-item-1');
        $this->assertTrue($cacheItem->isHit());
        $this->assertArraySubset(['test-tag-1', 'test-tag-2'], $cacheItem->getTags());
        unset($cacheItem);

        // 2 miss tags
        $this->cacheTagManager->save(
            $this->cacheTagManager->createItem('test-item-2')
                ->set('test-data-2')->setTags(['test-tag-2', 'test-tag-3'])
        );
        $this->cacheTagManager->save(
            $this->cacheTagManager->createItem('test-item-3')
                ->set('test-data-3')->setTags(['test-tag-3', 'test-tag-4'])
        );

        // 3 hit items + 2 hit tags (test-tag-1 and test-tag-2 already loaded)
        /** @var DataAdapterTaggedInterface[] $cacheItemCollection */
        $cacheItemCollection = $this->cacheTagManager->getItems(['test-item-1', 'test-item-2', 'test-item-3']);
        $this->assertTrue($cacheItemCollection['test-item-1']->isHit());
        $this->assertTrue($cacheItemCollection['test-item-2']->isHit());
        $this->assertTrue($cacheItemCollection['test-item-3']->isHit());
        unset($cacheItemCollection);

        $this->cacheTagManager->expireTags(['test-tag-2']); // rewrite test-tag-2 data, but keep loaded

        // 3 hit items (all tags already loaded)
        /** @var DataAdapterTaggedInterface[] $cacheItemCollection */
        $cacheItemCollection = $this->cacheTagManager->getItems(['test-item-1', 'test-item-2', 'test-item-3']);
        $this->assertFalse($cacheItemCollection['test-item-1']->isHit());
        $this->assertFalse($cacheItemCollection['test-item-2']->isHit());
        $this->assertTrue($cacheItemCollection['test-item-3']->isHit());
        unset($cacheItemCollection);

        $stats = $this->arrayCacheProvider->getStats();
        $this->assertEquals(11, $stats['hits']);
        $this->assertEquals(5, $stats['misses']);
    }

    /**
     * Test tagged item with deferred save
     */
    public function testDeferredTagsSingle()
    {
        $this->cacheTagManager->saveDeferred(
            $this->cacheTagManager->createItem('test-item-1')
                ->set('test-data-1')->setTags(['test-tag-1', 'test-tag-2'])
        );

        $cacheItem = $this->cacheTagManager->getItem('test-item-1');
        $this->assertFalse($cacheItem->isHit());
        unset($cacheItem);

        $this->assertTrue($this->cacheTagManager->commit());

        // 1 hit item + 2 hit tags
        $cacheItem = $this->cacheTagManager->getItem('test-item-1');
        $this->assertTrue($cacheItem->isHit());
        $this->assertArraySubset(['test-tag-1', 'test-tag-2'], $cacheItem->getTags());
        unset($cacheItem);
    }

    /**
     * Test deferred save for multiply tagged items
     */
    public function testDeferredTagsMultiply()
    {
        $this->cacheTagManager->saveDeferred(
            $this->cacheTagManager->createItem('test-item-1')
                ->set('test-data-1')->setTags(['test-tag-1', 'test-tag-2'])
        );

        $this->cacheTagManager->saveDeferred(
            $this->cacheTagManager->createItem('test-item-2')
                ->set('test-data-2')->setTags(['test-tag-2', 'test-tag-3'])
        );

        $this->cacheTagManager->saveDeferred(
            $this->cacheTagManager->createItem('test-item-3')
                ->set('test-data-3')->setTags(['test-tag-3', 'test-tag-4'])
        );

        /** @var DataAdapterTaggedInterface[] $cacheItemCollection */
        $cacheItemCollection = $this->cacheTagManager->getItems(['test-item-1', 'test-item-2']);
        $this->assertFalse($cacheItemCollection['test-item-1']->isHit());
        $this->assertFalse($cacheItemCollection['test-item-2']->isHit());
        unset($cacheItemCollection);

        $cacheItem = $this->cacheTagManager->getItem('test-item-3');
        $this->assertFalse($cacheItem->isHit());
        unset($cacheItem);

        $this->assertTrue($this->cacheTagManager->commit());

        $this->cacheTagManager->expireTags(['test-tag-4']);

        // 3 hit items + 2 hit tags (test-tag-1 and test-tag-2 already loaded)
        /** @var DataAdapterTaggedInterface[] $cacheItemCollection */
        $cacheItemCollection = $this->cacheTagManager->getItems(['test-item-1', 'test-item-2', 'test-item-3']);
        $this->assertTrue($cacheItemCollection['test-item-1']->isHit());
        $this->assertTrue($cacheItemCollection['test-item-2']->isHit());
        $this->assertFalse($cacheItemCollection['test-item-3']->isHit());
        unset($cacheItemCollection);
    }
}